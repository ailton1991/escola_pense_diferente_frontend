import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { TablesComponent } from '../../pages/tables/tables.component';

// import { AcessoLoginComponent } from '../../pages/escola_pense_diferente/acesso-login/acesso-login.component';
import { TabelaAlunosComponent } from '../../pages/escola_pense_diferente/tabela-alunos/tabela-alunos.component';
import { TabelaEscolasComponent } from '../../pages/escola_pense_diferente/tabela-escolas/tabela-escolas.component';
import { FormularioAlunosComponent } from '../../pages/escola_pense_diferente/formulario-alunos/formulario-alunos.component';
import { FormularioEscolasComponent } from '../../pages/escola_pense_diferente/formulario-escolas/formulario-escolas.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'tables',         component: TablesComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent },

    // { path: 'acesso-login',   component: AcessoLoginComponent },
    { path: 'tabela-alunos',   component: TabelaAlunosComponent },
    { path: 'tabela-escolas',   component: TabelaEscolasComponent },
    { path: 'formulario-alunos', component: FormularioAlunosComponent},
    { path: 'formulario-escolas', component: FormularioEscolasComponent},
];

