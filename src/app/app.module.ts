import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

// import { AcessoLoginComponent } from './pages/escola_pense_diferente/acesso-login/acesso-login.component';
import { TabelaEscolasComponent } from './pages/escola_pense_diferente/tabela-escolas/tabela-escolas.component';
import { TabelaAlunosComponent } from './pages/escola_pense_diferente/tabela-alunos/tabela-alunos.component';
import { FormularioEscolasComponent } from './pages/escola_pense_diferente/formulario-escolas/formulario-escolas.component';
import { FormularioAlunosComponent } from './pages/escola_pense_diferente/formulario-alunos/formulario-alunos.component';


@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    // AcessoLoginComponent,
    TabelaEscolasComponent,
    TabelaAlunosComponent,
    FormularioEscolasComponent,
    FormularioAlunosComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
