import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioEscolasComponent } from './formulario-escolas.component';

describe('FormularioEscolasComponent', () => {
  let component: FormularioEscolasComponent;
  let fixture: ComponentFixture<FormularioEscolasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioEscolasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormularioEscolasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
