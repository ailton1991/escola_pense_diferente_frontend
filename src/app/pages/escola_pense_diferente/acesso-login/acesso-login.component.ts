import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-acesso-login',
  templateUrl: './acesso-login.component.html',
  styleUrls: ['./acesso-login.component.css']
})

export class AcessoLoginComponent implements OnInit, OnDestroy {

  valorDoInputUsuario: string = '';
  valorDoInputSenha: string = '';
  showAlert: boolean;


  alertaUsuarioHeSenha : boolean;

  constructor(
    private router: Router,
    private modalService: NgbModal
  )
  {

  }

  ngOnInit() {
     this.alertaUsuarioHeSenha = false;
  }

  ngOnDestroy() {
  }

  btnAcessar(valorDoInputUsuario : any, valorDoInputSenha : any) {

    if(valorDoInputUsuario === 'TESTE' && valorDoInputSenha === '123')
    {
      this.router.navigate(['/tabela-escolas'], { queryParams: {  }});
      return true;
    }
    else
    {
      this.alertaUsuarioHeSenha = true;
      return true;
    }

  }

  fecharAlerta() {
    this.alertaUsuarioHeSenha = false;
    return true;
  }


}
