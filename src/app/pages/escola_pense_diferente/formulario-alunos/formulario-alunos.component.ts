import { Component, OnInit } from '@angular/core';
// import { Pipe, PipeTransform } from '@angular/core';
// import { NgControl } from '@angular/forms';

import { IEscolaDto } from 'src/app/dtos/escola.dto';
import { EscolaService } from 'src/app/services/escola.service';
import { IResponseMessageEscolaDTO } from 'src/app/dtos/response-message-escola.dto';

@Component({
  selector: 'app-formulario-alunos',
  templateUrl: './formulario-alunos.component.html',
  styleUrls: ['./formulario-alunos.component.css']
})
export class FormularioAlunosComponent implements OnInit {

  options = ['Escola 1', 'Escola 2', 'Escola 3'];
  selectedOption: string = 'A';

  dateOfBirth: string = '';

  private storage: Storage;

  keyListaEscolasStorage: string;

  _listaDeEscolasStorage: IEscolaDto[] = [];

  constructor(
    // private control: NgControl
    private escolaService: EscolaService,
  )
  {
    this.storage = window.localStorage;
  }

  ngOnInit(): void {

    this.loadListaEscolas();

  }

  loadListaEscolas(){

    this._listaDeEscolasStorage = [];

    this.escolaService.GetAll_Escolas().subscribe({

      next: (response: IResponseMessageEscolaDTO) => {

        if(response.isSuccess == false){

        }
        else if(response.isSuccess == true)
        {
          if(response.listaObjetosEscola.length > 0) {

            this.keyListaEscolasStorage = 'keyListaEscolasStorage';
            let listaEscolasStorage = JSON.stringify(response.listaObjetosEscola);
            this.storage.setItem(this.keyListaEscolasStorage, listaEscolasStorage);

            this._listaDeEscolasStorage = response.listaObjetosEscola;

            // this.statusSite = true;

            console.log('GetAllPerguntas:', this._listaDeEscolasStorage);

            return true;
          }
          else
          {


            return true;
          }
        }

      },
      error: error => {

        console.error('Error:', error);
        console.error('Error:', 'Deu ruim bino');

        return true;

      }

    });

  }

  formatDate(date: any): string {
    const day = date.day < 10 ? '0' + date.day : date.day;
    const month = date.month < 10 ? '0' + date.month : date.month;
    const year = date.year;
    return `${day}/${month}/${year}`;
  }

  transformCPF(value: string): string {
    if (!value) return value;
    value = value.replace(/\D/g, '');
    return value.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1.$2.$3-$4');
  }

}
