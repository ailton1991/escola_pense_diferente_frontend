import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelaEscolasComponent } from './tabela-escolas.component';

describe('TabelaEscolasComponent', () => {
  let component: TabelaEscolasComponent;
  let fixture: ComponentFixture<TabelaEscolasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabelaEscolasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TabelaEscolasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
