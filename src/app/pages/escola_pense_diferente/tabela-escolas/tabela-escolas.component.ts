import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';

import { IEscolaDto } from 'src/app/dtos/escola.dto';
import { EscolaService } from 'src/app/services/escola.service';
import { IResponseMessageEscolaDTO } from 'src/app/dtos/response-message-escola.dto';

@Component({
  selector: 'app-tabela-escolas',
  templateUrl: './tabela-escolas.component.html',
  styleUrls: ['./tabela-escolas.component.css']
})
export class TabelaEscolasComponent implements OnInit {

  private storage: Storage;

  keyListaEscolasStorage: string;

  _listaDeEscolasStorage: IEscolaDto[] = [];

  constructor(
    private escolaService: EscolaService
  )
  {
    this.storage = window.localStorage;
  }

  ngOnInit(): void {

    this.loadListaEscolas();

  }

  loadListaEscolas(){

    this._listaDeEscolasStorage = [];

    this.escolaService.GetAll_Escolas().subscribe({

      next: (response: IResponseMessageEscolaDTO) => {

        if(response.isSuccess == false){

        }
        else if(response.isSuccess == true)
        {
          if(response.listaObjetosEscola.length > 0) {

            this.keyListaEscolasStorage = 'keyListaEscolasStorage';
            let listaEscolasStorage = JSON.stringify(response.listaObjetosEscola);
            this.storage.setItem(this.keyListaEscolasStorage, listaEscolasStorage);

            this._listaDeEscolasStorage = response.listaObjetosEscola;

            // this.statusSite = true;

            console.log('GetAllPerguntas:', this._listaDeEscolasStorage);

            return true;
          }
          else
          {


            return true;
          }
        }

      },
      error: error => {

        console.error('Error:', error);
        console.error('Error:', 'Deu ruim bino');

        return true;

      }

    });

  }

  async btnFiltroEscola(event: Event) {

    await this.sleep(1400);

    const filtroEscola = (event.target as HTMLInputElement).value;

    if(filtroEscola === '' || filtroEscola === ""){

      this.loadListaEscolas();

    }
    else
    {

      this.escolaService.GetFilter_Escolas(filtroEscola).subscribe({

        next: (response: IResponseMessageEscolaDTO) => {

          if(response.isSuccess == false){

          }
          else if(response.isSuccess == true)
          {
            if(response.listaObjetosEscola.length > 0) {

              this.keyListaEscolasStorage = 'keyListaEscolasStorage';
              let listaEscolasStorage = JSON.stringify(response.listaObjetosEscola);
              this.storage.setItem(this.keyListaEscolasStorage, listaEscolasStorage);

              this._listaDeEscolasStorage = response.listaObjetosEscola;

              // this.statusSite = true;

              console.log('GetAllPerguntas:', this._listaDeEscolasStorage);

              return true;
            }
            else
            {


              return true;
            }
          }

        },
        error: error => {

          console.error('Error:', error);
          console.error('Error:', 'Deu ruim bino');

          return true;

        }



      });

    }

  }


  //##################################################
  //##### Início - Configurações e Consultas

  sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  //##### Fim - Configurações e Consultas
  //##################################################



}
