import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IResponseMessageEscolaDTO } from 'src/app/dtos/response-message-escola.dto';

@Injectable({
  providedIn: 'root'
})
export class EscolaService {

  constructor(
    private http: HttpClient,
) { }

  //https://localhost:44358/Escola/GetAll
  GetAll_Escolas() : Observable<IResponseMessageEscolaDTO> {

    let url = environment.apiCSharpUrlHTTPS +'/Escola/GetAll';
    return this.http.get<IResponseMessageEscolaDTO>(url,  {  });

  }

  //https://localhost:44358/Escola/GetAllPage?page=0&pageSize=10
  GetAllPage_Escolas(page: number, pageSize: number) : Observable<IResponseMessageEscolaDTO> {

    let url = environment.apiCSharpUrlHTTPS +'/Escola/GetAllPage?page=' + page + '&pageSize=' + pageSize;
    return this.http.get<IResponseMessageEscolaDTO>(url,  {  });

  }

  // https://localhost:44358/Escola/GetById?iCodEscola=
  GetById_Escolas(iCodAluno: string) : Observable<IResponseMessageEscolaDTO> {

    let url = environment.apiCSharpUrlHTTPS +'/Escola//GetById?iCodEscola=' + iCodAluno;
    return this.http.get<IResponseMessageEscolaDTO>(url,  {  });

  }

  // https://localhost:44358/Escola/GetFilter?sDescricao=Esplanada
  GetFilter_Escolas(sDescricao: string) : Observable<IResponseMessageEscolaDTO> {

    let url = environment.apiCSharpUrlHTTPS +'/Escola/GetFilter?sDescricao=' + sDescricao;
    return this.http.get<IResponseMessageEscolaDTO>(url,  {  });

  }

  // https://localhost:44358/Escola/Post?sDescricao=Esplanada
  Post_Escolas(sDescricao: string) : Observable<IResponseMessageEscolaDTO> {

    let url = environment.apiCSharpUrlHTTPS +'/Escola/Post?sDescricao=' + sDescricao;
    return this.http.post<IResponseMessageEscolaDTO>(url,  {  });

  }

  // https://localhost:44358/Escola/Put?iCodEscola=1&sDescricao=1
  Put_Escolas(iCodEscola : string, sDescricao: string) : Observable<IResponseMessageEscolaDTO> {

    let url = environment.apiCSharpUrlHTTPS +'/Escola/Put?iCodEscola=' + iCodEscola + '&sDescricao='+ sDescricao;
    return this.http.put<IResponseMessageEscolaDTO>(url,  {  });

  }

  // https://localhost:44358/Escola/Delete?iCodEscola=1
  Delete_Escolas(iCodEscola : string) : Observable<IResponseMessageEscolaDTO> {

    let url = environment.apiCSharpUrlHTTPS +'/Escola/Delete?iCodEscola=' + iCodEscola;
    return this.http.put<IResponseMessageEscolaDTO>(url,  {  });

  }

}

