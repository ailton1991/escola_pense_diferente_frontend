import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IResponseMessageAlunosDTO } from 'src/app/dtos/response-message-alunos.dto';

@Injectable({
  providedIn: 'root'
})
export class AlunosService {

  constructor(
    private http: HttpClient,
) { }

    //https://localhost:44358/Alunos/GetAll
  GetAll_Alunos() : Observable<IResponseMessageAlunosDTO> {

    let url = environment.apiCSharpUrlHTTPS +'/Alunos/GetAll';
    return this.http.get<IResponseMessageAlunosDTO>(url,  {  });

  }

  //https://localhost:44358/Alunos/GetAllPage?page=0&pageSize=10
  GetAllPage_Alunos(page: number, pageSize: number) : Observable<IResponseMessageAlunosDTO> {

    let url = environment.apiCSharpUrlHTTPS +'/Alunos/GetAllPage?page=' + page + '&pageSize=' + pageSize;
    return this.http.get<IResponseMessageAlunosDTO>(url,  {  });

  }

  // https://localhost:44358/Alunos/GetById?iCodAlunos=
  GetById_Alunos(iCodAluno: string) : Observable<IResponseMessageAlunosDTO> {

    let url = environment.apiCSharpUrlHTTPS +'/Alunos//GetById?iCodAlunos=' + iCodAluno;
    return this.http.get<IResponseMessageAlunosDTO>(url,  {  });

  }

  // https://localhost:44358/Alunos/GetFilter?sDescricao=Esplanada
  GetFilter_Alunos(sDescricao: string) : Observable<IResponseMessageAlunosDTO> {

    let url = environment.apiCSharpUrlHTTPS +'/Alunos/GetFilter?sDescricao=' + sDescricao;
    return this.http.get<IResponseMessageAlunosDTO>(url,  {  });

  }

  // https://localhost:44358/Alunos/Post?sNome=Ailton&dNascimento=03%2F10%2F1991&sCPF=1116&sEndereco=Rua&sCelular=21&iCodEscola=1
  Post_Alunos(sNome: string, dNascimento: string, sCPF : string, sEndereco : string, sCelular : string, iCodEscola : string) : Observable<IResponseMessageAlunosDTO> {

    let url = environment.apiCSharpUrlHTTPS +'/Alunos/Post?sNome='+sNome+'&dNascimento='+dNascimento+'&sCPF='+sCPF+'&sEndereco='+sEndereco+'&sCelular='+sCelular+'&iCodEscola='+iCodEscola;
    return this.http.post<IResponseMessageAlunosDTO>(url,  {  });

  }

  // https://localhost:44358/Alunos/Put?iCodAluno=1&sNome=Ailton&dNascimento=03%2F10%2F1991&sCPF=1116&sEndereco=Rua&sCelular=21&iCodEscola=1
  Put_Alunos(iCodAluno: string, sNome: string, dNascimento: string, sCPF : string, sEndereco : string, sCelular : string, iCodEscola : string) : Observable<IResponseMessageAlunosDTO> {

    let url = environment.apiCSharpUrlHTTPS +'/Alunos/Put?iCodAluno='+iCodAluno+'&sNome='+sNome+'&dNascimento='+dNascimento+'&sCPF='+sCPF+'&sEndereco='+sEndereco+'&sCelular='+sCelular+'&iCodEscola='+iCodEscola;
    return this.http.post<IResponseMessageAlunosDTO>(url,  {  });

  }

  // https://localhost:44358/Alunos/Delete?iCodAlunos=1
  Delete_Alunos(iCodAlunos : string) : Observable<IResponseMessageAlunosDTO> {

    let url = environment.apiCSharpUrlHTTPS +'/Alunos/Delete?iCodAlunos=' + iCodAlunos;
    return this.http.put<IResponseMessageAlunosDTO>(url,  {  });

  }



}
