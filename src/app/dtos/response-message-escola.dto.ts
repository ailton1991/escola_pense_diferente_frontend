export interface IResponseMessageEscolaDTO {

  isSuccess: boolean;
  message: string | undefined;
  messageConsole:  string | undefined;
  objetosEscola?: any| undefined;
  listaObjetosEscola?: any| undefined;

};
