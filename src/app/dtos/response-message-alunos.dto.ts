export interface IResponseMessageAlunosDTO {

  isSuccess: boolean;
  message: string | undefined;
  messageConsole:  string | undefined;
  objetosAlunos?: any| undefined;
  listaObjetosAlunos?: any| undefined;

};
