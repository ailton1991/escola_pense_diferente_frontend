import { IAlunosDto } from 'src/app/dtos/alunos.dto';

export interface IEscolaDto {

  iCodEscola: string;
  sDescricao: string;
  listaAlunos: IAlunosDto[];

}
