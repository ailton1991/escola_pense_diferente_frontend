export interface IAlunosDto {

  iCodAluno: string
  sNome: string
  dNascimento?: Date;
  sCPF: string
  sEndereco: string
  sCelular: string
  iCodEscola: string

}
